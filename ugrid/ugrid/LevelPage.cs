﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Xamarin.Forms;

namespace ugrid
{
    public class LevelPage : ContentPage
    {
        Button backButton, resetButton, nextButton, levelName;
        StackLayout stackLayout, buttonStack;
        GameBoard gameBoard;
        const int HELP_CNT = 3; // count of levels with help
        Color color; // color of this level
        int curLevel; // zero-indexed

        public LevelPage(Color color, int _curLevel)
        {
            //this.BackgroundColor = Color.Aqua;
            this.BackgroundColor = Color.FromRgb(1, 74, 127);
            curLevel = _curLevel;
            curLevel %= LevelDescriptor.Levels.Count;
            Title = "Ugrid";
            this.color = LevelDescriptor.Levels[curLevel].GoalColor;
            int x = LevelDescriptor.Levels[curLevel].Field.GetLength(0);
            int y = x;
            backButton = new Button
            {
                Text = "F",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                FontSize = 20,
                TextColor = this.BackgroundColor,
                BorderColor = Color.White,
                BorderRadius = 100,
                BackgroundColor = Color.White,
            };

            backButton.Clicked += BackButton_Click;

            Grid.SetColumn(backButton, 0);
            Grid.SetRow(backButton, 0);

            levelName = new Button
            {
                Text = LevelDescriptor.Levels[curLevel].Name,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 30,
                TextColor = Color.White,
                BorderColor = Color.White,
                BorderRadius = 10,
                BorderWidth = 1,
                BackgroundColor = this.color
                
            };

            levelName.IsEnabled = false;

            Grid.SetColumn(levelName, 1);
            Grid.SetRow(levelName, 0);

            resetButton = new Button
            {
                Text = "I",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                FontSize = 20,
                TextColor = this.BackgroundColor,
                BorderColor = Color.White,
                BorderRadius = 100,
                BackgroundColor = Color.White,
            };

            Grid.SetColumn(resetButton, 2);
            Grid.SetRow(resetButton, 0);
            resetButton.Clicked += ResetButton_Click;
            resetButton.Clicked += async (sender, e) => await LevelStart();

            nextButton = new Button()
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.FromRgb(1, 74, 127),
                Text = "h",
                FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                TextColor = Color.White,
                BorderColor = Color.White,
                BorderRadius = 3,
                BorderWidth = 3
            };
            nextButton.Clicked += NextButton_Click;
            nextButton.Clicked += async (sender, e) => await LevelStart();

            buttonStack = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start,
                Children = {
                    backButton,
                    levelName,
                    resetButton
                },
            };

            gameBoard = new GameBoard(color, LevelDescriptor.Levels[curLevel].Field, (curLevel < HELP_CNT));
            stackLayout = new StackLayout
            {
                Children =
                {
                   buttonStack,
                   new Grid {
                          HorizontalOptions = LayoutOptions.CenterAndExpand,
                          VerticalOptions = LayoutOptions.CenterAndExpand,
                          Children = {
                                gameBoard
                          }
                   }
                }
            };

            buttonStack.LayoutChanged += Page_SizeChanged;
            gameBoard.HorizontalOptions = LayoutOptions.CenterAndExpand;
            gameBoard.VerticalOptions = LayoutOptions.CenterAndExpand;
            this.SizeChanged += Page_SizeChanged;
            gameBoard.GameEnded += async (sender, e) => await LevelFinish();
            this.Appearing += async (sender, e) => await LevelStart();
            Content = stackLayout;
            this.LayoutChanged += Page_SizeChanged;
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            if ((stackLayout.Children[1] as Grid).Children.Count > 1)
                (stackLayout.Children[1] as Grid).Children.RemoveAt(1);
            gameBoard.HorizontalOptions = LayoutOptions.CenterAndExpand;
            gameBoard.VerticalOptions = LayoutOptions.CenterAndExpand;
            gameBoard.Set(LevelDescriptor.Levels[curLevel].GoalColor, LevelDescriptor.Levels[curLevel].Field);
            gameBoard.EnableGame();
            Page_SizeChanged(this, EventArgs.Empty);
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            curLevel++;
            curLevel %= LevelDescriptor.Levels.Count;
            (stackLayout.Children[1] as Grid).Children.RemoveAt(1);
            gameBoard.HorizontalOptions = LayoutOptions.CenterAndExpand;
            gameBoard.VerticalOptions = LayoutOptions.CenterAndExpand;
            gameBoard.Set(LevelDescriptor.Levels[curLevel].GoalColor, LevelDescriptor.Levels[curLevel].Field);
            levelName.Text = LevelDescriptor.Levels[curLevel].Name;
            levelName.BackgroundColor = LevelDescriptor.Levels[curLevel].GoalColor;
            Page_SizeChanged(this, EventArgs.Empty);
            if (curLevel >= HELP_CNT)
            {
                gameBoard.SetHelper(false);
            }
            gameBoard.EnableGame();
        }

        private async void BackButton_Click(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        async Task LevelStart()
        {
            gameBoard.Scale = 1.2;
            await (gameBoard).ScaleTo(1.0, 700, Easing.SpringOut);
        }

        async Task LevelFinish()
        {
            if (curLevel != LevelDescriptor.Levels.Count - 1 &&
                curLevel + 1 == (int)(App.Current.Properties["progress"]))
                App.Current.Properties["progress"] = (int)(App.Current.Properties["progress"]) + 1;

            gameBoard.DisableGame();
            (stackLayout.Children[1] as Grid).Children.Add(nextButton);
            Grid.SetRow(nextButton, 0);
            Grid.SetColumn(nextButton, 0);
            nextButton.Scale = 0;
            double playAgainButtonWidth = nextButton.Measure(Double.PositiveInfinity, Double.PositiveInfinity).Request.Width;
            double maxScale = stackLayout.Children[1].Width / 2 / playAgainButtonWidth;
            await nextButton.ScaleTo(maxScale, 555, Easing.SpringOut);
        }

        void Page_SizeChanged(object sender, EventArgs e)
        {
            double gameWidth = Width;
            double gameHeight = Height;


            if (Width > Height + 100)
            {
                stackLayout.Orientation = StackOrientation.Horizontal;
                buttonStack.Orientation = StackOrientation.Vertical;
                buttonStack.VerticalOptions = LayoutOptions.Center;
                buttonStack.HorizontalOptions = LayoutOptions.Start;

                levelName.WidthRequest = levelName.HeightRequest = 0.13 * gameWidth;
                backButton.WidthRequest = backButton.HeightRequest = 0.09 * gameWidth;
                resetButton.WidthRequest = resetButton.HeightRequest = 0.09 * gameWidth;

                int padding = Math.Max(10, Math.Min(150, 10 + (int)Width / 100));
                buttonStack.Padding = new Thickness(padding, 0, 0, 0);
                gameWidth = gameWidth * 0.7;
            }
            else
            {
                stackLayout.Orientation = StackOrientation.Vertical;
                buttonStack.Orientation = StackOrientation.Horizontal;
                buttonStack.VerticalOptions = LayoutOptions.Start;
                buttonStack.HorizontalOptions = LayoutOptions.Center;

                levelName.WidthRequest = levelName.HeightRequest = 0.13 * gameHeight;
                backButton.WidthRequest = backButton.HeightRequest = 0.09 * gameHeight;
                resetButton.WidthRequest = resetButton.HeightRequest = 0.09 * gameHeight;

                int padding = Math.Max(10, Math.Min(150, 10 + (int)Width / 100));
                buttonStack.Padding = new Thickness(0, padding, 0, 0);
                gameHeight = gameHeight * 0.7;
            }
            levelName.FontSize = levelName.Width / 2;
            backButton.FontSize = backButton.Width / 2;
            resetButton.FontSize = resetButton.Width / 2;
            buttonStack.Spacing = levelName.Width / 5;


            double squareSize = Math.Min(0.9 * gameWidth, 0.9 * gameHeight);

            for (int i = 0; i < gameBoard.Rows * gameBoard.Cols; i++)
            {
                (gameBoard.Children[i] as Tile).FontSize = squareSize / 6;
            }

            for (int i = 0; i < gameBoard.Rows; i++)
            {
                gameBoard.RowDefinitions[i].Height = squareSize / gameBoard.Rows;
            }
            for (int i = 0; i < gameBoard.Cols; i++)
            {
                gameBoard.ColumnDefinitions[i].Width = squareSize / gameBoard.Cols;
            }
        }
    }
}
