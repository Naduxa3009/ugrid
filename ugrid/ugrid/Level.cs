﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace ugrid
{
    public class ColorConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Color color = (Color)value;
            int red = (int)(color.R * 255);
            int green = (int)(color.G * 255);
            int blue = (int)(color.B * 255);
            string hex = String.Format("{0:X2}{1:X2}{2:X2}", red, green, blue);
            writer.WriteValue(hex);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            Color color = Color.FromHex((string)reader.Value);
            return color;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Color);
        }
    }
    public class FieldConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Boolean[,] Field = (Boolean[,])value;
            StringBuilder fieldStr = new StringBuilder();
            fieldStr.Append('[');
            for (int i = 0; i < Field.GetLength(0); i++)
            {
                fieldStr.Append("\n\t\t\t[ ");
                for (int j = 0; j < Field.GetLength(1); j++)
                {
                    if (Field[i, j])
                        fieldStr.Append("1");
                    else
                        fieldStr.Append("0");
                    if (j != Field.GetLength(1) - 1)
                        fieldStr.Append(", ");
                    else
                        fieldStr.Append(" ");
                }
                fieldStr.Append(']');
                if (i != Field.GetLength(0) - 1) fieldStr.Append(',');
            }
            fieldStr.Append("\n\t\t]");
            writer.WriteRawValue(fieldStr.ToString());
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Boolean[,]);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class Level
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("color"), JsonConverter(typeof(ColorConverter))]
        public Color GoalColor { get; set; }

        [JsonProperty("field"), JsonConverter(typeof(FieldConverter))]
        public bool[,] Field { get; set; }

        public Level()
        {
            this.Name = "0";
            this.GoalColor = Color.Red;
            this.Field = new bool [3, 3];
        }

        public Level(string name, Color color, bool[,] field)
        {
            this.Name = name;
            this.GoalColor = color;
            this.Field = field;
        }

        [JsonConstructor]
        public Level(string name, string color, int [,] field)
        {
            Name = name;
            GoalColor = Color.FromHex(color);
            Field = new bool[field.GetLength(0), field.GetLength(1)];

            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    Field[i, j] = (field[i,j] == 1);
                }
            }
        }

        public string toJson()
        {
            int red = (int)(GoalColor.R * 255);
            int green = (int)(GoalColor.G * 255);
            int blue = (int)(GoalColor.B * 255);
            return JsonConvert.SerializeObject(new Level("0", Color.FromRgb(red, green, blue), Field), Formatting.Indented);
        }
       
    }
}
