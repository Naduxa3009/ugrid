﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Newtonsoft.Json.Linq;

namespace ugrid
{
    
    public static class LevelDescriptor
    {
        public static List<Level> Levels { get; private set; }

        public static void Parse()
        {
            var assembly = typeof(LevelDescriptor).GetTypeInfo().Assembly; 
            
            Stream stream = assembly.GetManifestResourceStream("ugrid.levels.json");
            
            string text = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            Levels = JsonConvert.DeserializeObject<List<Level>>(text);
           
        }

    }
}
