﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ugrid
{
    class Board : Grid
    {
        public int Cols { get; private set; }
        public int Rows { get; private set; }

        public Board(Color color, bool[,] field)
        {
            BackgroundColor = Color.FromRgb(1, 74, 127);
            Cols = field.GetLength(0);
            Rows = field.GetLength(1);
            Set(color, field);
        }

        public void Set(Color color, bool[,] field)
        {
            Children.Clear();
            ColumnDefinitions.Clear();
            RowDefinitions.Clear();
            ColumnSpacing = 3;
            RowSpacing = 3;

            for (int j = 0; j < field.GetLength(0); j++)
            {
                RowDefinitions.Add(new RowDefinition());
            }
            for (int i = 0; i < field.GetLength(1); i++)
            {
                ColumnDefinitions.Add(new ColumnDefinition());
            }
            Rows = field.GetLength(0);
            Cols = field.GetLength(1);
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    var stack1 = new Tile()
                    {
                        DefaultColor = BackgroundColor,
                        GoalColor = color,
                        BorderColor = Color.White,
                        BorderWidth = 3,
                        BorderRadius = 6,
                        FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                        FontSize = 20,
                        TextColor = Color.White
                    };

                    if (field[i, j])
                        stack1.SetOn();
                    else
                        stack1.SetOff();
                    
                    stack1.Clicked += Click;

                    SetRow(stack1, i);
                    SetColumn(stack1, j);
                    Children.Add(stack1);
                }
            }
           
        }
        
        protected int GetIndexTile(int x, int y)
        {
            return (x * Cols + y);
        }

        //If puzzle no solution return -1, else return right index tap to win (minimal steps solution)
        public int Solve()
        {
            int[] nmask = new int[(1 << this.Cols)];
            int[] bits = new int[(1 << this.Cols)];
            int[] lastBit = new int[(1 << this.Cols)];

            nmask[0] = 0;
            lastBit[0] = -1;

            for (int i = 1; i < (1 << this.Cols); i++)
            {
                bits[i] = bits[i >> 1] + (i & 1);
                
                for (int j = Cols - 1; j >= 0; j--)
                    if ((i & (1 << j)) != 0)
                    {
                        lastBit[i] = j;
                        nmask[i] ^= (1 << j);
                        if (j + 1 < Cols) nmask[i] ^= (1 << (j + 1));
                        if (j - 1 >= 0) nmask[i] ^= (1 << (j - 1));
                    }
            }
            int[] ma = new int[Rows + 1];
            for (int i = 0; i < Rows; i++)
            {
                ma[i] = 0;
                for (int j = 0; j < Cols; j++)
                    if (!((Tile)Children[i * Cols + j]).isOn())
                        ma[i] |= (1 << j);
            }
            
            int ans = int.MaxValue;
            int stepRow = -1, stepCol = -1;

            //Brute first row ans then restore all cells
            for (int mask = 0; mask < (1 << Cols); mask++)
            {
                int tempStepRow = -1, tempStepCol = -1;
                if (lastBit[mask] != -1)
                {
                    tempStepRow = 0;
                    tempStepCol = lastBit[mask];
                }
                int tek = bits[mask];
                int last1 = (ma[0] ^ nmask[mask]);
                int last2 = (ma[1] ^ mask);

                for (int i = 1; i < Rows; i++)
                {
                    if (lastBit[last1] != -1 && tempStepRow == -1)
                    {
                        tempStepRow = i;
                        tempStepCol = lastBit[last1];
                    }
                    int last3 = (ma[i + 1] ^ last1);
                    last2 ^= nmask[last1];
                    tek += bits[last1];
                    last1 = last2;
                    last2 = last3;
                }
                if (last1 == 0 && tek < ans)
                {
                    ans = tek;
                    stepCol = tempStepCol;
                    stepRow = tempStepRow;
                }
            }
            if (ans == int.MaxValue)
                return -1;
            if (ans == 0)
                return -2;
            return GetIndexTile(stepRow, stepCol);
        }

        public Level GetCurrentLevel()
        {
            Level level = new Level();
            level.Field = new bool[Rows, Cols];

            level.GoalColor = ((Tile)Children[0]).GoalColor;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    level.Field[i, j] = ((Tile)Children[i * Cols + j]).isOn();
                }
            }
            return level;
        }
        protected virtual void Click(object sender, EventArgs e)
        {
            (sender as Tile).Change();
        }
    }
}
