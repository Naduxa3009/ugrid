﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;
using Xamarin.Forms.PlatformConfiguration;

namespace ugrid
{
    public class MenuPage : ContentPage
    {
        ScrollView ScrollContainer;
        Grid grid;
        int nextLockLevel; //zero-indexed
        Frame logo;
        
        public MenuPage() {
            var  assembly = typeof(MenuPage).GetTypeInfo().Assembly;
            Image logoImage = new Image {
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.Center,
                Source = ImageSource.FromResource("ugrid.logo.png", assembly)
            };

            /* uncomment this for unlock Constructor Level in two taps logo
             * 
            var profileTapRecognizer = new TapGestureRecognizer
            {
                Command = new Command(() => {
                    StartConstuctor(null, EventArgs.Empty);
                }),
                NumberOfTapsRequired = 2
            };
            logoImage.GestureRecognizers.Add(profileTapRecognizer);
            */

            logo = new Frame
            {
                Content = logoImage,
                OutlineColor = Color.White,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.Fill,
                HasShadow = true,
                Margin = 0,
                BackgroundColor = Color.FromRgb(1, 74, 127)
            };
           
            BackgroundColor = Color.FromRgb(1, 74, 127);
            ScrollContainer = new ScrollView
            {
                Orientation = ScrollOrientation.Vertical,
            };
            LevelDescriptor.Parse();
            grid = new Grid();
            grid.Children.Add(logo);
            grid.ColumnSpacing = 15;
            grid.RowSpacing = 15;
            Random rand = new Random();

            object progress = "";

            //progress in Properties is Next First Lock Level !

            //App.Current.Properties["progress"] = 1; /// reset progress every launch for test

            if (!App.Current.Properties.TryGetValue("progress", out progress))
            {
                App.Current.Properties["progress"] = 1;
            }
            nextLockLevel = 0;
            for (int i = 0; i < LevelDescriptor.Levels.Count; i++)
            {
                Button button = new Button
                {
                    BorderRadius = Device.OnPlatform<int>(iOS: 0, Android: 1, WinPhone: 1),
                    BorderWidth = 1,
                    BorderColor = Color.White,
                    Text = "B".ToString(), // lock
                    TextColor = Color.White,
                    FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                    FontAttributes = FontAttributes.Bold,
                    BackgroundColor = this.BackgroundColor
                };

                grid.Children.Add(button);
            }


            //ScrollContainer.Margin = new Thickness(0, 40, 0, 0);
            ScrollContainer.Padding = new Thickness(0, 40, 0, 60);
            ScrollContainer.Content = grid;
            Content = ScrollContainer;
            this.Appearing += async (sender, e) => await UpdateProgress();
            this.SizeChanged += Page_SizeChanged;
            this.Appearing += Page_SizeChanged;
        }

        void Page_SizeChanged(object sender, EventArgs e)
        {
            int cols = (Width > Height + 100) ? 4 : 3;
            int rows = (LevelDescriptor.Levels.Count + (cols - 1)) / cols;    //autocount rows

            int squareSize = Math.Max(60, Math.Min(((int)Width - 120) / cols, ((int)Height - 120) / cols));

            for (int i = 0; i < LevelDescriptor.Levels.Count; i++)
            {
                Grid.SetRow(grid.Children[i + 1], 1 + i / cols);
                Grid.SetColumn(grid.Children[i + 1], i % cols);
            }
            grid.ColumnDefinitions.Clear();
            grid.RowDefinitions.Clear();
            for (int i = 0; i < cols; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
                grid.ColumnDefinitions[i].Width = squareSize;
            }

            Grid.SetRow(logo, 0);
            Grid.SetColumn(logo, 0);
            Grid.SetColumnSpan(logo, cols);

            for (int i = 0; i < 1 + rows; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
                grid.RowDefinitions[i].Height = squareSize;
            }
            grid.RowDefinitions[0].Height = 1.7 * squareSize;


            logo.HeightRequest = (double)grid.RowDefinitions[0].Height.Value;
            logo.WidthRequest = squareSize * cols + (cols - 1) * grid.ColumnSpacing;

            for (int i = 1; i < grid.Children.Count; i++)
            {
                (grid.Children[i] as Button).FontSize = squareSize / 2.5;
            }

            int leftMargin = (int)(Width - squareSize * cols - grid.ColumnSpacing * (cols - 1)) / 2;
            grid.Padding = new Thickness(leftMargin, 0, 0, 0);
        }

        public async void StartConstuctor(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ConstructorPage());
        }

        public async void StartGame(object sender, EventArgs e)
        {
            int curLevel = grid.Children.IndexOf((sender as Button)) - 1;
            await Navigation.PushModalAsync(new LevelPage(((sender) as Button).BackgroundColor, curLevel));
        }

        public async Task UpdateProgress()
        {
            while (nextLockLevel < (int)Application.Current.Properties["progress"])
            {
                Button button = (grid.Children[1 + nextLockLevel] as Button);
                button.Text = LevelDescriptor.Levels[nextLockLevel % LevelDescriptor.Levels.Count].Name;
                button.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                button.ClearValue(Button.FontFamilyProperty); ;
                
                button.BackgroundColor = LevelDescriptor.Levels[nextLockLevel % LevelDescriptor.Levels.Count].GoalColor;
                button.Clicked += StartGame;
                nextLockLevel++;
            }
            await Task.Delay(300);
            if (nextLockLevel == 1)
                await ScrollContainer.ScrollToAsync(grid.Children[0], ScrollToPosition.Center, true);
            else
                await ScrollContainer.ScrollToAsync(grid.Children[nextLockLevel], ScrollToPosition.Center, true);
        }
    }
}
