﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace ugrid
{
    class Tile:Button
    {
        public Color GoalColor { get; set; }
        public Color DefaultColor { get; set; }

        public bool isOn()
        {
            return BackgroundColor == GoalColor;
        }

        public void SetOn()
        {
            BackgroundColor = GoalColor;
        }
        public void SetOff()
        {
            BackgroundColor = DefaultColor;
        }
        public void Change()
        {
            if (isOn())
                SetOff();
            else
                SetOn();
        }

        public static explicit operator Tile(Task<bool> v)
        {
            throw new NotImplementedException();
        }
    }
}
