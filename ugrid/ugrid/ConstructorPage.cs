﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Xamarin.Forms;


namespace ugrid
{
    public class ConstructorPage : ContentPage
    {
        Button backButton, resetButton, applyButton;
        StackLayout stackLayout, buttonStack;
        Board board;
        Color color; // color of this level
        Entry colsEntry, rowsEntry;
        Entry colorField;
        public ConstructorPage()
        {
            this.BackgroundColor = Color.FromRgb(1, 74, 127);
            color = Color.Orange;
            backButton = new Button
            {
                Text = "F",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                FontSize = 20,
                TextColor = this.BackgroundColor,
                BorderColor = Color.White,
                BorderRadius = 100,
                BackgroundColor = Color.White,
            };

            backButton.Clicked += BackButton_Click;
            applyButton = new Button
            {
                Text = "Generate",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                //FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                FontSize = 20,
                TextColor = this.BackgroundColor,
                BorderColor = Color.White,
                BorderRadius = 60,
                BackgroundColor = Color.White
            };
            applyButton.Clicked += ApplyButton_Click;
            Grid.SetColumn(backButton, 0);
            Grid.SetRow(backButton, 0);


            Grid.SetColumn(backButton, 1);
            Grid.SetRow(backButton, 0);

            resetButton = new Button
            {
                Text = "I",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                FontSize = 20,
                TextColor = this.BackgroundColor,
                BorderColor = Color.White,
                BorderRadius = 100,
                BackgroundColor = Color.White,
            };

            colsEntry = new Entry();
            rowsEntry = new Entry();

            Grid.SetColumn(resetButton, 2);
            Grid.SetRow(resetButton, 0);
            Grid.SetColumn(rowsEntry, 0);
            Grid.SetRow(rowsEntry, 1);
            Grid.SetColumn(colsEntry, 0);
            Grid.SetRow(colsEntry, 1);
            resetButton.Clicked += ResetButton_Click;
            resetButton.Clicked += async (sender, e) => await LevelStart();


            buttonStack = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start,
                Children = {
                    backButton,
                    resetButton,
                    rowsEntry,
                    colsEntry
                },
            };

            rowsEntry.Text = "3";
            colsEntry.Text = "3";
            colorField = new Entry();
            colorField.Text = ((int)(color.R * 255)).ToString() + " " + ((int)(color.G * 255)) + " " + ((int)(color.B * 255));
            board = new Board(color, new bool[3, 3]);
            stackLayout = new StackLayout
            {
                Children =
                {
                   buttonStack,
                   colorField,
                   applyButton,
                   new Grid {
                          HorizontalOptions = LayoutOptions.CenterAndExpand,
                          VerticalOptions = LayoutOptions.CenterAndExpand,
                          Children = {
                                board
                          }
                   }
                }
            };

            buttonStack.LayoutChanged += Page_SizeChanged;
            board.HorizontalOptions = LayoutOptions.CenterAndExpand;
            board.VerticalOptions = LayoutOptions.CenterAndExpand;
            this.SizeChanged += Page_SizeChanged;
            this.Appearing += async (sender, e) => await LevelStart();
            Content = stackLayout;
            this.LayoutChanged += Page_SizeChanged;
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            if ((stackLayout.Children[3] as Grid).Children.Count > 1)
                (stackLayout.Children[3] as Grid).Children.RemoveAt(1);
            board.HorizontalOptions = LayoutOptions.CenterAndExpand;
            board.VerticalOptions = LayoutOptions.CenterAndExpand;
            int cols, rows;
            try
            {
                cols = int.Parse(colsEntry.Text);
                rows = int.Parse(rowsEntry.Text);
            }
            catch
            {
                colsEntry.Text = board.Cols.ToString();
                rowsEntry.Text = board.Rows.ToString();
                return;
            }

            Color col = BackgroundColor;
            try
            {
                string[] rgbs = colorField.Text.Split(' ');
                col = Color.FromRgb(int.Parse(rgbs[0]), int.Parse(rgbs[1]), int.Parse(rgbs[2]));
                colorField.BackgroundColor = col;
            }
            catch
            {
            }
            board.Set(col, new bool[rows, cols]);
            Page_SizeChanged(this, EventArgs.Empty);
        }

        private async void BackButton_Click(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private async void ApplyButton_Click(object sender, EventArgs e)
        {

            string info;
            if (board.Solve() != -1) info = board.GetCurrentLevel().toJson();
            else info = "INVALID FIELD";
            await Navigation.PushModalAsync(new LevelShowPage(info));
        }

        async Task LevelStart()
        {
            board.Scale = 1.7;
            await (board).ScaleTo(1.0, 700, Easing.SpringOut);
        }

        void Page_SizeChanged(object sender, EventArgs e)
        {
            double gameWidth = Width;
            double gameHeight = Height;


            if (Width > Height + 100)
            {
                stackLayout.Orientation = StackOrientation.Horizontal;
                buttonStack.Orientation = StackOrientation.Vertical;
                buttonStack.VerticalOptions = LayoutOptions.Center;
                buttonStack.HorizontalOptions = LayoutOptions.Start;

                backButton.WidthRequest = backButton.HeightRequest = 0.09 * gameWidth;
                resetButton.WidthRequest = resetButton.HeightRequest = 0.09 * gameWidth;

                int padding = Math.Max(10, Math.Min(150, 10 + (int)Width / 100));
                buttonStack.Padding = new Thickness(padding, 0, 0, 0);
                gameWidth = gameWidth * 0.7;
            }
            else
            {
                stackLayout.Orientation = StackOrientation.Vertical;
                buttonStack.Orientation = StackOrientation.Horizontal;
                buttonStack.VerticalOptions = LayoutOptions.Start;
                buttonStack.HorizontalOptions = LayoutOptions.Center;

                backButton.WidthRequest = backButton.HeightRequest = 0.09 * gameHeight;
                resetButton.WidthRequest = resetButton.HeightRequest = 0.09 * gameHeight;

                int padding = Math.Max(10, Math.Min(150, 10 + (int)Width / 100));
                buttonStack.Padding = new Thickness(0, padding, 0, 0);
                gameHeight = gameHeight * 0.7;
            }
            backButton.FontSize = backButton.Width / 2;
            resetButton.FontSize = resetButton.Width / 2;
            buttonStack.Spacing = backButton.Width / 5;


            double squareSize = Math.Min(0.9 * gameWidth, 0.9 * gameHeight);



            for (int i = 0; i < board.Rows; i++)
            {
                board.RowDefinitions[i].Height = squareSize / board.Rows;
            }
            for (int i = 0; i < board.Cols; i++)
            {
                board.ColumnDefinitions[i].Width = squareSize / board.Cols;
            }
        }
    }

    public class LevelShowPage : ContentPage
    {
        public LevelShowPage(string info)
        {
            BackgroundColor = Color.FromRgb(1, 74, 127);
            var entry = new Editor
            {

                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand

            };
            entry.Text = info;
            entry.WidthRequest = 100;
            entry.HeightRequest = 300;
            var backButton = new Button
            {
                Text = "F",
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
                FontFamily = Device.OnPlatform(null, "customfont.ttf#customfont", "/Assets/Fonts/customfont.ttf#customfont"),
                FontSize = 20,
                TextColor = this.BackgroundColor,
                BorderColor = Color.White,
                BackgroundColor = Color.Black,
            };
            backButton.Clicked += OnDismissButtonClicked;

            Content = new StackLayout
            {
                Children = {
                        backButton,
                        entry,
                }
            };
        }
        async void OnDismissButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PopModalAsync();
        }
    }
}