﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ugrid
{
    class GameBoard : Board
    {
        int goalCount;
        bool helper;
        public event EventHandler GameEnded; // event to LevelPage
        private event EventHandler Clicked; // click any tile or enable game

        public GameBoard(Color color, bool[,] field, bool needHelper) : base(color, field)
        {
            SetHelper(needHelper);
            goalCount = 0;
            for (int i = 0; i < Rows * Cols; i++)
            {
                if ((Children[i] as Tile).isOn())
                {
                    goalCount++;
                }
            }
            DoHelpAnimation();
        }
        
        public void EnableGame()
        {
            goalCount = 0;
            for (int i = 0; i < Rows * Cols; i++)
            {
                Children[i].IsEnabled = true;
                if ((Children[i] as Tile).isOn())
                {
                    goalCount++;
                }
            }
            Clicked(null, EventArgs.Empty);
        }

        public void DisableGame()
        {
            for (int i = 0; i < Rows * Cols; i++)
            {
                Children[i].IsEnabled = false;
            }
        }

        public void SetHelper(bool help)
        {
            helper = help;
            
        }

        int DoHelp(int nextStep)
        {
            if (nextStep >= 0 && nextStep < Rows * Cols)
            {
                (Children[nextStep] as Tile).Text = "";
            }
            if (!helper) return -1;
            nextStep = Solve();
            if (nextStep >= 0 && nextStep < Rows * Cols)
            {
                (Children[nextStep] as Tile).Text = "!";
            }
            return nextStep;
        }

        async Task DoHelpAnimation()
        {
            int nextStep = DoHelp(-1);

            Clicked += (sender, e) => {
                nextStep = DoHelp(nextStep);
            };
          
            while (helper)
            {
                if (nextStep >= 0 && nextStep < Rows * Cols)
                {
                    await AnimateHelper(Children[nextStep] as Tile);
                }
                await Task.Delay(4000);
            }
        }
      
        async Task AnimateHelper(Tile tile)
        {
            await tile.ScaleTo(0.90, 800, Easing.SpringOut);
            await tile.ScaleTo(1.0, 800, Easing.SpringOut);
        }

        async Task AnimateTile(Tile tile)
        {
           await tile.ScaleTo(0.95, 170);
           await tile.ScaleTo(1.0, 170);
        }

        protected override void Click(object sender, EventArgs e)
        {
            AnimateTile(sender as Tile);
            foreach (var indexTile in GetNeighbors(sender as Tile))
            {
                ((Tile)Children[indexTile]).Change();
                if (((Tile)Children[indexTile]).isOn())
                    goalCount++;
                else
                    goalCount--;
            }

            if (goalCount == Cols * Rows)
                GameEnded(this, EventArgs.Empty);

            if (helper)
                Clicked(null, EventArgs.Empty);
        }

        List<int> GetNeighbors(Tile tile)
        {
            List<int> listNeighbors = new List<int>();
            int x = GetRow(tile), y = GetColumn(tile);
            listNeighbors.Add(GetIndexTile(x, y));

            if (x - 1 >= 0)
            {
                listNeighbors.Add(GetIndexTile(x - 1, y));
            }
            if (x + 1 < Rows)
            {
                listNeighbors.Add(GetIndexTile(x + 1, y));
            }
            if (y - 1 >= 0)
            {
                listNeighbors.Add(GetIndexTile(x, y - 1));
            }
            if (y + 1 < Cols)
            {
                listNeighbors.Add(GetIndexTile(x, y + 1));
            }
            return listNeighbors;
        }
    }
}