using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(Button), typeof(ugrid.Droid.FlatButtonRenderer))]

namespace ugrid.Droid
{
    public class FlatButtonRenderer : ButtonRenderer
    {
        protected override void OnDraw(Android.Graphics.Canvas canvas)
        {
            base.OnDraw(canvas);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                if (!(Control as Android.Widget.Button).Enabled)
                {
                    (Control as Android.Widget.Button).SetTextColor(Android.Content.Res.ColorStateList.ValueOf(Android.Graphics.Color.White));
                }
            }
        }
    }
}